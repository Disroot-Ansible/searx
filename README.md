# SearxNG - Ansible Role

This role covers deployment, configuration and software updates of SearXNG. This role is released under MIT License and we give no warranty for this piece of software. Currently supported OS - Debian. Although it has started as SearX role, ever since we have migrated to SearxNG on Disroot, **Searx flavor of the role isn't being tested and thus not supported!**

Role Provides all needed variables in the `defaults/main.yml`. Do not change any values in that file. If you wish to customize and tweak your instance, copy needed variables to your `host_vars` file.

## Themes and dashboard
Role supports installation of custom themes such as [searxng-beetroot](https://git.disroot.org/Disroot-themes/searxng-beetroot) which is a default theme on our [SearxNG instance](https://search.disroot.org). Here is an example of how to add beetroot theme to your instance:
```
searx_custom_themes:
  - name: 'beetroot' #add name of the theme
    repo: 'https://git.disroot.org/Disroot-themes/searxng-beetroot.git' #add git repository of the theme
    version: 'main' # git branch to use
    static_folder: 'beetroot_static' #add static folder name
    template_folder: 'beetroot_template' #add template folder name
searxng_default_theme: 'beetroot'
```

This theme additionally provides a neat possibility to add extra links in the **topbar** menu as well as *"dashboard"*. Dashboard is a list of shortcuts to other apps served on the platform. Both of those can be configured in SearXNG settings file so no additional theme forking needed to adjust to your liking.
Icons used by dashboard are located in beetroot's static directory:`beetroot_static/img/dashboard/`.
Icons used in topbar are using ionicons used by searxng. 

Here is how you can adjust your host_vars:
```
## Searx extras brand variables
searx_extra_topbar: 'true' #set to true to display additional link on the top bar
searx_topbar_items:
  - name: 'Server Status'
    url: 'https://status.example.com'
    icon: 'magnet-outline'
searx_dashboard: 'true' #set to true if you want to display app links below search bar
searx_dashboard_apps:
  - name: 'Webmail'
    url: 'https://webmail.example.com'
    icon: 'email_circle.png'
  - name: 'Cloud'
    url: 'https://cloud.example.com'
    icon: 'cloud_circle.png'
```
### Theme update
You can use the role to update your custom themes only. To do that specify `--tags theme` eg: `ansible-playbook Playbooks/searx.yml --tags theme`

## Testing on Vagrant 
To test on vagrant, simply run `vagrant up` and once the container is provisioned, run `ansible-playbook -b Playbooks/searx.yml`. SearxNG is served from `192.168.33.10` port `80` (`http://192.168.33.10`). Please note default playbook requires our [nginx](https://git.disroot.org/Disroot-Ansible/nginx) as a dependency. It is not mandatory when running outside provided VagrantFile. 


## SearXNG or SearX?
Although not tested do practically not supported, you can choose to deploy either SearX or SearXNG by setting `searx_or_searxng` variable. Default is set to `searxng` as it's what we currently use on Disroot.
Additionally you can also decide to deploy **filtron** for **SearX** or **limiter** for **SearXNG**. Default is set to deploy `limiter` for `SearXNG`.
Since neither **SearX** nor **filtron** are no longer supported by us, use at your own risk!

